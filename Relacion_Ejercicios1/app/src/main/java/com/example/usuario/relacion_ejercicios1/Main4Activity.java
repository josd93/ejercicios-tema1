package com.example.usuario.relacion_ejercicios1;
import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Main4Activity extends AppCompatActivity implements View.OnClickListener {

    Button menos, mas, comenzar, renovar;
    TextView contadorCafes, contadorTiempo;
    int cafes = 0, tiempo = 5;
    MyCountDownTimer contadorDescendente;
    MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);
        menos = (Button) findViewById(R.id.button1);
        mas = (Button) findViewById(R.id.button2);
        comenzar = (Button) findViewById(R.id.button3);
        menos.setOnClickListener(this);
        mas.setOnClickListener(this);
        comenzar.setOnClickListener(this);
        contadorCafes = (TextView) findViewById(R.id.textView2);
        contadorTiempo = (TextView) findViewById(R.id.textView3);
        ponerContadorTiempo(5);
        ponerContadorCafes(9);
        mp = MediaPlayer.create(this, R.raw.nokia);
        renovar = (Button) findViewById(R.id.renovar);
        renovar.setVisibility(View.INVISIBLE);
        renovar.setOnClickListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main4, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void onClick(View v) {
        if(v == comenzar){
            contadorDescendente = new MyCountDownTimer(tiempo * 60 * 1000, 1000);
            contadorDescendente.start();
            menos.setEnabled(false);
            mas.setEnabled(false);
            comenzar.setEnabled(false);
        }
        if(v == menos){
            ponerContadorTiempo(tiempo - 1);
        }
        if(v == mas){
            ponerContadorTiempo(tiempo + 1);
        }
        if(v == renovar){
            restablecer();
        }
    }

    private void restablecer() {
        ponerContadorCafes(0);
        menos.setEnabled(true);
        mas.setEnabled(true);
        comenzar.setEnabled(true);
        renovar.setVisibility(View.INVISIBLE);
    }


    public class MyCountDownTimer extends CountDownTimer {
        public MyCountDownTimer(long startTime, long interval) {
            super(startTime, interval);
        }
        @Override
        public void onTick(long millisUntilFinished) {
            long minutos = (millisUntilFinished/1000)/60;
            long segundos = (millisUntilFinished/1000)%60;
            contadorTiempo.setText(minutos + ":" + segundos);
        }
        @Override
        public void onFinish() {

            if(cafes > 8){
                renovar.setVisibility(View.VISIBLE);
                mp.start();
                AlertDialog.Builder popup = new AlertDialog.Builder(Main4Activity.this);
                popup.setTitle("FIN!!!!");
                popup.setMessage("Has tomado 10 cafés ya no puedes tomar mas");
                popup.setPositiveButton("Ok", null);
                popup.create();
                popup.show();
            }else{
                menos.setEnabled(true);
                mas.setEnabled(true);
                comenzar.setEnabled(true);
                mp.start();
            }
            ponerContadorCafes(cafes + 1);
            contadorTiempo.setText("Pausa terminada!!");
        }


    }
    private void ponerContadorCafes(int i) {
        cafes = i;
        contadorCafes.setText(String.valueOf(cafes));
    }

    private void ponerContadorTiempo(int t) {
        if(t < 1){
            t = 1;
        }
        tiempo = t;
        contadorTiempo.setText(tiempo + ":00");
    }
}
