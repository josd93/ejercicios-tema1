package com.example.usuario.relacion_ejercicios1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button btn1,btn2,btn3,btn4,btn5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn1 = (Button) findViewById(R.id.button1);
        btn2 = (Button) findViewById(R.id.button2);
        btn3 = (Button) findViewById(R.id.button3);
        btn4 = (Button) findViewById(R.id.button4);
        btn5 = (Button) findViewById(R.id.button5);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    // Metodo para cambiar de una actividad a otra.
    public void pasarActividad (View v){
        if(v == btn1){
            Intent act = new Intent(this,Main1Activity.class);
            startActivity(act);
        }
        if(v == btn2){
            Intent act = new Intent(this,Main2Activity.class);
            startActivity(act);
        }
        if(v == btn3){
            Intent act = new Intent(this,Main3Activity.class);
            startActivity(act);
        }
        if(v == btn4){
            Intent act = new Intent(this,Main4Activity.class);
            startActivity(act);
        }
        if(v == btn5){
            Intent act = new Intent(this,Main5Activity.class);
            startActivity(act);
        }

    }
}
