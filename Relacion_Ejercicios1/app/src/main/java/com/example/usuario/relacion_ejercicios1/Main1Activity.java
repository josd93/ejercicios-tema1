package com.example.usuario.relacion_ejercicios1;

import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

public class Main1Activity extends AppCompatActivity implements View.OnClickListener {

    EditText dolares;
    EditText euros;
    EditText cambioIntroducido;
    RadioButton rbd;
    RadioButton rbe;
    Button calcular;
    double cambio;
    Conversion miConversor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main1);

        dolares = (EditText)findViewById(R.id.editText);
        euros = (EditText)findViewById(R.id.editText2);
        cambioIntroducido = (EditText)findViewById(R.id.editText3);
        rbd = (RadioButton) findViewById(R.id.radioButtonDolares);
        rbd.setChecked(true);
        rbe = (RadioButton) findViewById(R.id.radioButtonEuros);
        calcular = (Button) findViewById(R.id.button3);
        calcular.setOnClickListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main1, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if(cambioIntroducido.getText().toString().isEmpty()){
            cambio = 0;
            miConversor = new Conversion(cambio);
        }else{
            cambio = Double.parseDouble(cambioIntroducido.getText().toString());
            miConversor = new Conversion(cambio);
        }


        if(v == calcular){
            if(rbd.isChecked()){
                String valor = dolares.getText().toString();
                if (valor.isEmpty()){
                    valor = "0";
                }
                euros.setText(miConversor.convertirAEuros(valor));
            }
            if(rbe.isChecked()){
                String valor = euros.getText().toString();
                if (valor.isEmpty()){
                    valor = "0";
                }
                dolares.setText(miConversor.convertirADolares(valor));
            }
        }
    }
}
