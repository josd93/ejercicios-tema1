package com.example.usuario.relacion_ejercicios1;

/**
 * Created by usuario on 29/09/15.
 */
public class Conversion {

    private double cambio;
    //CONSTRUCTOR
    public Conversion() {
        this.cambio = 0.89;
    }

    public Conversion(double c) {
        this.cambio = c;
    }
    //METODOS
    public String convertirAEuros(String cantidad) {
        double valor = Double.parseDouble(cantidad) * cambio;
        return
                String.format
                        ("%.2f", valor);
    }

    public String convertirADolares(String cantidad) {
        if(cambio == 0){
            double valor = 0;
            return
                    String.format
                            ("%.2f", valor);
        }else{
            double valor = Double.parseDouble(cantidad) / cambio;
            return
                    String.format
                            ("%.2f", valor);
        }


    }
}
