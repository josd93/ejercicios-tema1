package com.example.usuario.relacion_ejercicios1;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Main5Activity extends AppCompatActivity implements View.OnClickListener {
    EditText num1,num2,num3;
    TextView result;
    Button ordenar;
    double a, b, c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main5);
        num1 = (EditText) findViewById(R.id.numero1);
        num2 = (EditText) findViewById(R.id.numero2);
        num3 = (EditText) findViewById(R.id.numero3);
        result = (TextView)findViewById(R.id.resultado);
        ordenar = (Button) findViewById(R.id.ordenar);
        ordenar.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main5, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        //variable para no perder los datos al ordenarlos.
        double aux;
        //Comprobamos que se hayan rellenado todas las casillas, si no se pone a 0.
        if(num1.getText().toString().isEmpty()){
            a = 0;
        }else{
            a = Double.parseDouble(num1.getText().toString());
        }
        if(num2.getText().toString().isEmpty()){
            b = 0;
        }else{
            b = Double.parseDouble(num2.getText().toString());
        }
        if(num3.getText().toString().isEmpty()){
            c = 0;
        }else{
            c = Double.parseDouble(num3.getText().toString());
        }
        // ordenación de los dos primeros números
        if (a > b) {
            aux = a;
            a = b;
            b = aux;
        }
        // ordenación de los dos últimos números
        if (b > c) {
            aux = b;
            b = c;
            c = aux;
        }
        // se vuelven a ordenar los dos primeros
        if (a > b) {
            aux = a;
            a = b;
            b = aux;
        }
        result.setText("Los números introducidos ordenados de menor a mayor son " + a+", " + b + "y " + c);



    }
}
